<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Todolist;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // $todolists = Todolist::all();
        // foreach($todolists as $x => $todolist){
        //     echo $todolist->user->name . '<br>';
        // }

        // $todolists = Todolist::with('user')->get();
        $todolists = Todolist::with('user')->orderBy('id', 'desc')->paginate(20);

        return view('todolist.home', compact('todolists'));
    }

    public function create(){
        return view('todolist.create');
    }

    public function store(Request $request){
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'due_date' => 'bail|required|date',
        ],[
            'due_date.required' => 'Sila masukkan tarikh akhir',
            'due_date.date' => 'Sila semak jenis data',
        ]);

        $loggedUser = Auth::user();

        $todolist = new Todolist();
        $todolist->title = $request->title;
        $todolist->description = $request->description;
        $todolist->user_id = $loggedUser->id;
        $todolist->due_date = $request->due_date;
        $todolist->created_at = Carbon::now();
        $todolist->save();
    return redirect()->route('home')->with('success', 'Anda telah berjaya');

    }

    public function view(){
        $user = User::find(1);
        $todolist = new Todolist([
            'title' => 'A new todolist.',
            'description' => 'Jimot add this',
            'due_date' => Carbon::now()
        ]);
        $comment = $user->todolists()->save($todolist);

    }

    public function edit($id){
        $todolist = Todolist::find($id);
        $users = User::all();
        return view('todolist.edit', compact('todolist', 'users'));
    }

    public function update(Request $request){
        $loggedUser = Auth::user();
        // find todolist by its ID
        $todolist = Todolist::find($request->todolistId);
        $todolist->title = $request->title;
        $todolist->description = $request->description;
        $todolist->user_id = $request->author;
        $todolist->due_date = $request->due_date;
        $todolist->updated_at = Carbon::now();
        $todolist->updated_by = $loggedUser->id;
        $todolist->save();
        return redirect()->route('home');
    }

    public function delete($id){
        $todolist = Todolist::find($id);
        $todolist->delete();
        return redirect()->route('home');
    }
}
