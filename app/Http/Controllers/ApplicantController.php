<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreApplicantRequest;
use App\Http\Requests\UpdateApplicantRequest;
use App\Models\Applicant;
use App\Models\Academic;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class ApplicantController extends Controller
{
    public function index()
    {
        $applicants = Applicant::all();
        return view('applicants.index', compact('applicants'));
    }

    public function create()
    {
        return view('applicants.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'ic' => 'required',
            'dob' => 'bail|required|date',
        ],[
            'dob.required' => 'Sila masukkan tarikh akhir',
            'dob.date' => 'Sila semak jenis data',
        ]);

        $loggedUser = Auth::user();

        // Calculate age using CARBON
        $age = Carbon::parse($request->dob)->age;


        // PARENT
        $applicant = new Applicant();
        $applicant->name = $request->name;
        $applicant->ic = $request->ic;
        $applicant->dob = $request->dob;
        $applicant->address = $request->address;
        $applicant->created_at = Carbon::now();
        $applicant->save();

        // PARENT ID / Foreign key on academic's model
        $applicantId = $applicant->id;

        // CHILD
        $academic = new Academic();
        $academic->category = $request->category;
        $academic->name = $request->certificate;
        $academic->applicant_id = $applicantId; // include parent_id
        $academic->save();

        return redirect()->route('applicant.edit', $applicantId)->with('success', 'Anda telah berjaya');
    }

    public function edit($id)
    {
        $applicant = Applicant::with('academics')->where('id', $id)->get()->first();
        return view('applicants.edit', compact('applicant'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateApplicantRequest  $request
     * @param  \App\Models\Applicant  $applicant
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateApplicantRequest $request, Applicant $applicant)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Applicant  $applicant
     * @return \Illuminate\Http\Response
     */
    public function destroy(Applicant $applicant)
    {
        //
    }

    public function storeAcademic(Request $request){
        $academic = new Academic();
        $academic->category = $request->category2;
        $academic->name = $request->certificate2;
        $academic->fileupload = $request->certificate_file2;
        $academic->applicant_id = $request->applicantId;
        $academic->created_at = Carbon::now();
        $academic->save();
        return redirect()->back();

    }
}
