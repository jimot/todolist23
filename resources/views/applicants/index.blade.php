@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <h5>Job Applicants</h5>
            <div class="col-md-12 mt-2 mb-3">
                <a class="btn btn-primary btn-sm" href="{{ route('applicant.add') }}">Add new application</a>
            </div>

            @if ($applicants->isNotEmpty())
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Ic</th>
                            <th>DOB</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($applicants as $key => $app)
                        <tr>
                            <td>{{ ++$key }}.</td>
                            <td>{{ $app->name }}</td>
                            <td>{{ $app->ic }}</td>
                            <td>{{ $app->dob }}</td>
                            <td>
                                <a href="{{ route('applicant.edit', $app->id) }}">Edit</a>
                                <a href="">Delete</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <p class="text-danger">No application yet</p>
            @endif
        </div>
    </div>
@endsection
