@extends('layouts.app')

@section('content')
<div class="container">
    <form method="post" action="{{ route('applicant.store') }}">
        @csrf
        <div class="row">
            <div class="cold-md8 card">
                <div class="p-3 card-title">
                    Personal Information
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Name:</label>
                            <input class="form-control" type="text" name="name" value="{{ $applicant->name }}" placeholder="Please enter name">
                        </div>
                        <div class="col-md-6">
                            <label>Ic No.:</label>
                            <input class="form-control" type="text" name="ic" value="{{ $applicant->ic }}" placeholder="Please enter ic">
                        </div>
                        <div class="col-md-6">
                            <label>Tarikh Lahir:</label>
                            <input class="form-control" type="date" name="dob" value="{{ $applicant->dob }}" placeholder="Please enter dob">
                        </div>
                        <div class="col-md-6">
                            <label>Age:</label>
                            <input class="form-control" type="text" name="age" value="{{ $applicant->age }}" placeholder="Please enter age">
                        </div>
                        <div class="col-md-6">
                            <label>Address:</label>
                            <textarea class="form-control">{{ $applicant->address }}</textarea>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card mt-3">
                <div class="card-body">
                    Academic Information
                    @foreach($applicant->academics as $academic)
                        <div class="academic mt-5">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Category:</label>
                                    <select class="form-control" id="selectCategory" name="category">
                                        <option value="">--Please choose category--</option>
                                        <option value="1" {{ $academic->category == 1 ? 'selected' : '' }}>SPM</option>
                                        <option value="2" {{ $academic->category == 2 ? 'selected' : '' }}>STPM</option>
                                        <option value="3" {{ $academic->category == 3 ? 'selected' : '' }}>Diploma</option>
                                        <option value="4" {{ $academic->category == 4 ? 'selected' : '' }}>Sarjana Muda</option>

                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label>Certificate Name:</label>
                                    <input class="form-control" type="text" name="certificate" value="{{ $academic->name }}" placeholder="Please enter certificate name">
                                </div>
                                <div class="col-md-6">
                                    <label>Certificate:</label>
                                    <input class="form-control" type="file" name="certificate_file" value="{{ $applicant->certificate }}" placeholder="Please enter certificate name">
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <div class="addMore mt-3">
                        <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#exampleModal">Add More Academic</button>
                        <a href="{{ route('applicant') }}" class="btn btn-warning">Home</a>
                    </div>
                </div>
            </div>
        </div>
        {{--  <button type="submit" class="mt-3 btn btn-primary">SUBMIT</button>  --}}
        {{--  TODO  --}}
        {{--  loop academic & update all including its parent (personal info)  --}}
    </form>
</div>

{{--  Modal Form Add Academic  --}}
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="{{ route('academic.store') }}">
                @csrf
                <input type="hidden" name="applicantId" value="{{ $applicant->id }}">
                <div class="modal-body">
                    <div class="col-md-12">
                        <label>Category:</label>
                        <select class="form-control" id="selectCategory" name="category2">
                            <option value="">--Please choose category--</option>
                            <option value="1">SPM</option>
                            <option value="2">STPM</option>
                            <option value="3">Diploma</option>
                            <option value="4">Sarjana Muda</option>
                        </select>
                    </div>
                    <div class="col-md-12">
                        <label>Certificate Name:</label>
                        <input class="form-control" type="text" name="certificate2" value="{{ old('certificate2') }}" placeholder="Please enter certificate name">
                    </div>
                    <div class="col-md-12">
                        <label>Certificate:</label>
                        <input class="form-control" type="file" name="certificate_file2" value="{{ old('certificate_file2') }}" placeholder="Please enter certificate name">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

{{--  include javascript  --}}
@push('jsscript')
    {{--  <script>
        $('#exampleModal').modal('toggle')

    </script>  --}}
@endpush
