@extends('layouts.app')

@section('content')
<div class="container">
    <form method="post" action="{{ route('applicant.store') }}">
        @csrf
        <div class="row">
            <div class="cold-md8 card">
                <div class="p-3 card-title">
                    Application Form
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Name:</label>
                            <input class="form-control" type="text" name="name" value="{{ old('name') }}" placeholder="Please enter name">
                        </div>
                        <div class="col-md-6">
                            <label>Ic No.:</label>
                            <input class="form-control" type="text" name="ic" value="{{ old('ic') }}" placeholder="Please enter ic">
                        </div>
                        <div class="col-md-6">
                            <label>Tarikh Lahir:</label>
                            <input class="form-control" type="date" name="dob" value="{{ old('dob') }}" placeholder="Please enter dob">
                        </div>
                        <div class="col-md-6">
                            <label>Age:</label>
                            <input class="form-control" type="text" name="age" value="{{ old('ic') }}" placeholder="Please enter age">
                        </div>
                        <div class="col-md-6">
                            <label>Address:</label>
                            <textarea class="form-control"></textarea>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card mt-3">
                <div class="card-body">
                    Academic Info
                    <div class="row mt-3">
                        <div class="col-md-6">
                            <label>Kategori:</label>
                            <select class="form-control" id="selectCategory" name="category">
                                <option value="">--Please choose category--</option>
                                <option value="1">SPM</option>
                                <option value="2">STPM</option>
                                <option value="3">Diploma</option>
                                <option value="4">Sarjana Muda</option>

                            </select>
                        </div>
                        <div class="col-md-6">
                            <label>Certificate Name:</label>
                            <input class="form-control" type="text" name="certificate" value="{{ old('certificate') }}" placeholder="Please enter certificate name">
                        </div>
                        <div class="col-md-6">
                            <label>Certificate:</label>
                            <input class="form-control" type="file" name="certificate_file" value="{{ old('certificate') }}" placeholder="Please enter certificate name">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <button type="submit" class="mt-3 btn btn-primary">SUBMIT</button>
    </form>
</div>

@endsection
