@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <a class="btn btn-danger" style="float:right;" href="{{ route('todolist.create') }}">New Todolist</a>
            <div class="red">
                testttt
            </div>
            <table class="table">
                <thead>
                    <th>#</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Author</th>
                    <th>Action</th>
                </thead>
                <tbody>
                    @foreach ($todolists as $key => $todolist )
                    <tr>
                        <td>{{ ++$key }}</td>
                        <td>{{ $todolist->title }}</td>
                        <td>{{ $todolist->description }}</td>
                        <td>{{ $todolist->user->name }}</td>
                        <td>
                            <a class="btn btn-warning" href="{{ route('todolist.edit', $todolist->id) }}">Edit</a>
                            <form method="post" action="{{ route('todolist.delete', $todolist->id) }} ">
                                @csrf
                                <input name="_method" type="hidden" value="DELETE">
                                <button type="button"  class="btn btn-danger">Delete</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

            {{ $todolists->links() }}
        </div>
    </div>
</div>
@endsection
