@extends('layouts.app')

@section('content')
    <form method="POST" action="{{ route('todolist.update') }}">
        @csrf
        <input type="hidden" name="todolistId" value="{{ $todolist->id }}">
        <div class="row p-5">
            <div class="col-md-6">
                <label>Title:</label>
                <input class="form-control" type="text" name="title" value="{{ $todolist->title }}">
                @error('title')
                    <small class="text-danger">Sila masukkan title</small>
                @enderror
            </div>
            <div class="col-md-6">
                <label>Due Date:</label>
                <input class="form-control" type="date" name="due_date" value="{{ $todolist->due_date }}">
                @error('due_date')
                    {{--  <small class="text-danger">Sila masukkan due date</small>  --}}
                    <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="col-md-6">
                <label>Description:</label>
                <textarea class="form-control" name="description">{{ $todolist->description }}</textarea>
                @error('description')
                    <small class="text-danger">Sila masukkan maklumat</small>
                @enderror
            </div>

            <div class="col-md-6">
                <label>Author:</label>
                <select class="form-control" id="selectUser" name="author">
                    <option value="">--Please choose user--</option>
                    @foreach ($users as $key => $user)
                        <option value="{{ $user->id }}" {{ $user->id == $todolist->user->id ? 'selected' : '' }}>
                            {{ $user->name }}</option>
                    @endforeach
                </select>
                @error('author')
                    <small class="text-danger">Sila masukkan author</small>
                @enderror
            </div>

            <button type="submit" class="btn btn-warning mt-3">Submit</button>
        </div>
    </form>
@endsection
