@extends('layouts.app')

@section('content')
    <form method="POST" action="{{ route('todolist.store') }}">
        @csrf
        <div class="row p-5">
            <div class="col-md-6">
                <label>Title:</label>
                <input class="form-control" type="text" name="title" value="{{ old('title') }}">
                @error('title')
                    <small class="text-danger">Sila masukkan title</small>
                @enderror
            </div>
            <div class="col-md-6">
                <label>Due Date:</label>
                <input class="form-control" type="date" name="due_date" value="{{ old('due_date') }}">
                @error('due_date')
                    {{--  <small class="text-danger">Sila masukkan due date</small>  --}}
                    <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="col-md-6">
                <label>Description:</label>
                <textarea class="form-control" name="description" value="{{ old('description') }}"></textarea>
                @error('description')
                    <small class="text-danger">Sila masukkan maklumat</small>
                @enderror
            </div>

            <button type="submit" class="btn btn-warning mt-3">Submit</button>
        </div>
    </form>
@endsection
