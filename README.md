# Welcome to Laravel!

Hello SCENICers! I'm your **LARAVEL's** guru. I'll grant you a yellow belt after mastering this. Hohoho!


# How to start?

$ git clone  git@gitlab.com:jimot/todolist23.git

$ composer install

$ npm install (if you want to use VITE build tool)

Copy **env.example** file to **.env**

Edit database/project info on **.env** file

$ php artisan key:generate

$ npm run dev (if you are using VITE build tool)

